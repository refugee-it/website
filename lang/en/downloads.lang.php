<?php
/* Copyright (C) 2016-2019 Stephan Kreutzer
 *
 * This file is part of refugee-it.de.
 *
 * refugee-it.de is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * refugee-it.de is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with refugee-it.de. If not, see <http://www.gnu.org/licenses/>.
 */



define("LANG_PAGETITLE", "Downloads");
define("LANG_HEADER", "Downloads");
define("LANG_HEADER_LANGSAM_GESPROCHENE_NACHRICHTEN", "Langsam Gesprochene Nachrichten of the Deutsche Welle");
define("LANG_DESCRIPTION_LANGSAM_GESPROCHENE_NACHRICHTEN", "The “%sLangsam Gesprochenen Nachrichten%s” (news spoken slowly) of the state radio broadcaster for abroad “%sDeutsche Welle%s” has a daily news segment which is spoken slowly. They also provide the corresponding text to read along. With the following program, you can download the text and audio recording of this broadcast. Java needs to be installed. You have to extract the Zip archive and execute the script %s$/langsam_gesprochene_nachrichten_1_gnu.sh%s if on a unix-like operating system such as GNU/Linux, then a new directory “%slangsam_gesprochene_nachrichten%s” will appear with the result.");
define("LANG_HEADER_SCHREIBUEBUNGSBLAETTER", "Training Pages for Writing");
define("LANG_DESCRIPTION_SCHREIBUEBUNGSBLAETTER", "Training pages for writing the German alphabet.");
define("LANG_SCHREIBUEBUNGSBLAETTER_COMMIT_2_SIZE", "8.9");
define("LANG_SCHREIBUEBUNGSBLAETTER_COMMIT_3_SIZE", "22.2");
define("LANG_HEADER_NOTESYSTEM", "Note Management System");
define("LANG_DESCRIPTION_NOTESYSTEM", "With the online note management system, one can compose notes about persons and attach documents to them, so volunteers can organize their help for people in a refugee camp or data of course attendees. Also, there’s a slightly outdated <a href=\"https://www.bitchute.com/video/il4KSccowCyG/\">demo video</a> (in German language) about the system.");
define("LANG_HEADER_WIKTIONARY", "German Nouns With Article");
define("LANG_DESCRIPTION_WIKTIONARY", "Lists of German nouns with article, version 2018-05-01.");
define("LANG_WIKTIONARY_XHTML_DE_AR_NOUNS_ARTICLE", "German nouns with article and Arabic translation");
define("LANG_WIKTIONARY_XHTML_DE_AR_NOUNS_ARTICLE_UNIQUE", "Unique German nouns with article and Arabic translation");
define("LANG_WIKTIONARY_XHTML_DE_NOUNS_ARTICLE", "German nouns with article");
define("LANG_WIKTIONARY_XHTML_DE_NOUNS_ARTICLE_UNIQUE", "Unique German nouns with article");
define("LANG_WIKTIONARY_XHTML_DE_NOUNS_ARTICLE_DER", "German nouns with male article");
define("LANG_WIKTIONARY_XHTML_DE_NOUNS_ARTICLE_DER_UNIQUE", "Unique German nouns with male article");
define("LANG_WIKTIONARY_XHTML_DE_NOUNS_ARTICLE_DIE", "German nouns with female article");
define("LANG_WIKTIONARY_XHTML_DE_NOUNS_ARTICLE_DIE_UNIQUE", "Unique German nouns with female article");
define("LANG_WIKTIONARY_XHTML_DE_NOUNS_ARTICLE_DAS", "German nouns with neuter article");
define("LANG_WIKTIONARY_XHTML_DE_NOUNS_ARTICLE_DAS_UNIQUE", "Unique German nouns with neuter article");



?>
